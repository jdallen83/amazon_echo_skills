from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session

import trivia_questions as tq


app = Flask(__name__)
ask = Ask(app, "/")

TQ = tq.TriviaQuestionsSQLite()


def generate_question(header='', t=TQ):
    qquestion = TQ.get_question()
    session.attributes['question'] = qquestion
    resp = "{}. . The category is . . {}. . . For {}. . . {}".format(header, qquestion['category'].lower(), qquestion['value'], qquestion['question'])
    resp = resp.replace('"', '').replace("'", '').replace('.', ',')
    return question(resp)


@ask.launch
def new_game():
    return generate_question(header='Welcome to Jeffpardy.')


@ask.intent("AnswerIntent")
def opinion_response(answer):
    if not isinstance(answer, basestring):
        return question("I didn't upderstand that. Maybe you should speak more clearly. Try Again?")

    if answer in ['quit', 'bye', 'goodbye']:
        return statement('Peace out yo.')

    cur_question = session.attributes['question']

    correct = tq.evaluate_guess(answer, cur_question['answer'])

    if correct:
        return statement('Correct, {}! You got {} Jeff Bucks!'.format(cur_question['answer'], cur_question['value']))
    else:
        return question("{}? Ha. Try again genius.".format(answer))


if __name__ == '__main__':
    app.run(debug=True, port=8302)
