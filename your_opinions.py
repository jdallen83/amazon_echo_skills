from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session


app = Flask(__name__)
ask = Ask(app, "/")


@ask.launch
def new_game():
    welcome_msg = render_template('opinion_welcome')
    return question(welcome_msg)


@ask.intent("OpinionIntent")
def opinion_response(name):
    if not isinstance(name, basestring):
        return "I didn't upderstand that. Maybe you should speak more clearly."
    response = "I don't know nothing about {}".format(name)
    if name.lower() == 'greg':
        response = "I think Greg needs to mind his business. And he never looks fresh. How hard is it to just mind your business and look fresh? Lame."
    elif name.lower() == "laura":
        response = "I really wish Laura would take off that ram mask thing. She's had it on all damn day."
    elif name.lower() == "andy":
        response = "Andy has some mad game. Using it all day."
    elif name.lower() == "grandma":
        response = "Grandma has the funkiest, freshest rhymes. I don't know how she does it."
    elif name.lower() == "jeff":
        response = "Jeff is one bad ass mother fucker. Jeff for president."
    #opinion_msg = render_template('opinion_response', response=response)
    return statement(response)


#@ask.intent("AnswerIntent", convert={'first': int, 'second': int, 'third': int})
#def answer(first, second, third):
#    winning_numbers = session.attributes['numbers']
#    if [first, second, third] == winning_numbers:
#        msg = render_template('win')
#    else:
#        msg = render_template('lose')

#    return statement(msg)


if __name__ == '__main__':
    app.run(debug=True, port=8301)