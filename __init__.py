import os

CONFIG = {}

CONFIG['ROOTDIR'] = os.path.dirname(os.path.realpath(__file__))
CONFIG['LOGFILE'] = os.path.join(CONFIG['ROOTDIR'], 'logfile.txt')
CONFIG['LOGGING_LEVEL'] = 0.0#logging.DEBUG