#from amazon_echo_skills.cthulu_cast_prep import get_scored_sample_df
#import pandas as pd
#from amazon_echo_skills import CONFIG
from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session


#SAMPLES_FILE = join(CONFIG['ROOTDIR'], 'cthulu_cast_samples.tsv')
RAIN_SAMPLES = ['Major flooding stone extensive streets of the strange stench was undecayed.',
                'The Flood threat will come from the old stories of the horrible companions',
                'Homes flooded windows and hideous specimens were so dissected along the sea.',
                'Heavy rain and flooding is the more period of the creatures and experience.']
COLD_SAMPLES = ['Winter storm watches have such a persistent specimens of his strange conduct',
                'Winter storm watches with the horror and distant deal downward desperation.',
                'Its the first day of astronomical wintered with any secret cult was unrest.',
                'A pattern change will bring brutally cold specimens of maniac of first time.']
SEL_SAMPLES = ['Major flooding stone extensive streets of the strange stench was undecayed.',
               'The Flood threat will come from the old stories of the horrible companions',
               'Homes flooded windows and hideous specimens were so dissected along the sea.',
               'Its the first day of astronomical wintered with any secret cult was unrest.',
               'A pattern change will bring brutally cold specimens of maniac of first time.']

app = Flask(__name__)
ask = Ask(app, "/")


def get_samples(rain_samples=RAIN_SAMPLES, cold_samples=COLD_SAMPLES, sel_samples=SEL_SAMPLES):
    return sel_samples[randint(0, len(sel_samples)-1)]
    rain_sample = rain_samples[randint(0, len(rain_samples)-1)]
    cold_sample = cold_samples[randint(0, len(cold_samples)-1)]
    if randint(0, 1) == 0:
        return rain_sample + ' ' + cold_sample
    else:
        return cold_sample + ' ' + rain_sample


@ask.launch
def new_game():
    welcome_msg = get_samples()
    return statement(welcome_msg)


@ask.intent("OpinionIntent")
def opinion_response(name):
    welcome_msg = get_samples()
    return statement(welcome_msg)
    
    if not isinstance(name, basestring):
        return "I didn't upderstand that. Maybe you should speak more clearly."
    response = "I don't know nothing about {}".format(name)
    if name.lower() == 'greg':
        response = "I think Greg needs to mind his business. And he never looks fresh. How hard is it to just mind your business and look fresh? Lame."
    elif name.lower() == "laura":
        response = "I really wish Laura would take off that ram mask thing. She's had it on all damn day."
    elif name.lower() == "andy":
        response = "Andy has some mad game. Using it all day."
    elif name.lower() == "grandma":
        response = "Grandma has the funkiest, freshest rhymes. I don't know how she does it."
    elif name.lower() == "jeff":
        response = "Jeff is one bad ass mother fucker. Jeff for president."
    #opinion_msg = render_template('opinion_response', response=response)
    return statement(response)


#@ask.intent("AnswerIntent", convert={'first': int, 'second': int, 'third': int})
#def answer(first, second, third):
#    winning_numbers = session.attributes['numbers']
#    if [first, second, third] == winning_numbers:
#        msg = render_template('win')
#    else:
#        msg = render_template('lose')

#    return statement(msg)


if __name__ == '__main__':
    app.run(debug=True, port=8303)
