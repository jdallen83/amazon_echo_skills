import sqlite3
from os.path import isfile, join
import random
from pymongo import MongoClient

#from messenger_bot import CONFIG
from pysimplelog import Logger
logger = Logger(__name__)#, logFileBasename=CONFIG['LOGFILE'], )

ROOTDIR = '/home/jefall17/messenger.seventysigma.com/messenger_bot'#CONFIG['ROOTDIR']
SQLITE_DB = './TRIVIAQS_PROC.sqlite3.db'
DB_LOG = """CREATE TABLE IF NOT EXISTS trivia_questions
        (id INT PRIMARY KEY NOT NULL,
        category TEXT,
        value INT,
        question TEXT,
        answer TEXT,
        round TEXT,
        air_date TEXT,
        show_number TEXT);"""
SQLITE_COLUMNS = ['id', 'category', 'value', 'question', 'answer', 'round', 'air_date', 'show_number']
MAX_QUESTIONS = 216929

MONGO_HOST = '127.0.0.1'
MONGO_PORT = 27017
MONGO_DB = 'messengerBot'
COLL_DATA = 'jeopardyQuestions'


STOPWORDS = [u'i', u'me', u'my', u'myself', u'we', u'our', u'ours', u'ourselves', u'you', u'your', u'yours', u'yourself', u'yourselves', u'he', u'him', u'his', u'himself', u'she', u'her', u'hers', u'herself', u'it', u'its', u'itself', u'they', u'them', u'their', u'theirs', u'themselves', u'what', u'which', u'who', u'whom', u'this', u'that', u'these', u'those', u'am', u'is', u'are', u'was', u'were', u'be', u'been', u'being', u'have', u'has', u'had', u'having', u'do', u'does', u'did', u'doing', u'a', u'an', u'the', u'and', u'but', u'if', u'or', u'because', u'as', u'until', u'while', u'of', u'at', u'by', u'for', u'with', u'about', u'against', u'between', u'into', u'through', u'during', u'before', u'after', u'above', u'below', u'to', u'from', u'up', u'down', u'in', u'out', u'on', u'off', u'over', u'under', u'again', u'further', u'then', u'once', u'here', u'there', u'when', u'where', u'why', u'how', u'all', u'any', u'both', u'each', u'few', u'more', u'most', u'other', u'some', u'such', u'no', u'nor', u'not', u'only', u'own', u'same', u'so', u'than', u'too', u'very', u's', u't', u'can', u'will', u'just', u'don', u'should', u'now']

ALPHANUM = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ']

"""
Compute the Damerau-Levenshtein distance between two given
strings (s1 and s2)
https://www.guyrutenberg.com/2008/12/15/damerau-levenshtein-distance-in-python/
"""
def damerau_levenshtein_distance(s1, s2, norm=False):
    d = {}
    lenstr1 = len(s1)
    lenstr2 = len(s2)
    for i in xrange(-1,lenstr1+1):
        d[(i,-1)] = i+1
    for j in xrange(-1,lenstr2+1):
        d[(-1,j)] = j+1
 
    for i in xrange(lenstr1):
        for j in xrange(lenstr2):
            if s1[i] == s2[j]:
                cost = 0
            else:
                cost = 1
            d[(i,j)] = min(
                           d[(i-1,j)] + 1, # deletion
                           d[(i,j-1)] + 1, # insertion
                           d[(i-1,j-1)] + cost, # substitution
                          )
            if i and j and s1[i]==s2[j-1] and s1[i-1] == s2[j]:
                d[(i,j)] = min (d[(i,j)], d[i-2,j-2] + cost) # transposition
 
    if not norm:
        return d[lenstr1-1,lenstr2-1]
    else:
        return float(d[lenstr1-1,lenstr2-1]) / float(max(len(s1), len(s2)))


def normalize_text(text, allowed_chars=set(ALPHANUM), stopwords=set(STOPWORDS)):
    text = text.replace('-', ' ')
    text = u''.join([c for c in text.strip().lower() if c in allowed_chars]).strip()
    text = u' '.join([t for t in text.split(' ') if not t in stopwords])
    return text


def split_answers(answer):
    if ' or ' in answer:
        answers = answer.split(' or ')
        return [split_answers(answers[0]), split_answers(answers[1])]
    elif ' Or ' in answer:
        answers = answer.split(' Or ')
        return [split_answers(answers[0]), split_answers(answers[1])]
    elif ' OR ' in answer:
        answers = answer.split(' OR ')
        return [split_answers(answers[0]), split_answers(answers[1])]
    elif '(' in answer:
        l = answer.find('(')
        r = answer.find(')')
        if r == -1 or r < l:
            return answer
        return [split_answers(answer[:l]), split_answers(answer[l+1:r]), split_answers(answer[r+1:])]
    else:
        return [answer]


def flatten_list(l):
    r = []
    for e in l:
        if isinstance(e, list):
            r = r + flatten_list(e)
        else:
            r.append(e)
    return r


def get_answer_list(answer):
    return [a for a in flatten_list(split_answers(answer)) if len(a)]


def evaluate_guess(guess, answer):
    resp = None

    valid_answers = [normalize_text(a) for a in get_answer_list(answer)]
    guess = normalize_text(guess)
    correct = False
    for a in valid_answers:
        if a in guess:
            correct = True
            break
        if damerau_levenshtein_distance(a, guess, norm=True) < 0.25:
            correct = True
            break

    logger.debug("EVAL: {} {} {} {}".format(correct, guess, answer, valid_answers))
    return correct


class TriviaQuestionsSQLite:
    conn = None
    db_file = None

    def __init__(self, db_file=join(ROOTDIR, SQLITE_DB), fields=SQLITE_COLUMNS):
        self.fields = fields
        self.db_file = db_file

    def connect(self):
        if self.conn is None:
            self.conn = sqlite3.connect(self.db_file)

    def get_question(self):
        self.connect()
        rows = []
        c = 0
        while not len(rows) and c < 10:
            rows = list(self.conn.execute('SELECT * FROM trivia_questions WHERE id=?', (random.randint(0, MAX_QUESTIONS), )))
            c += 1

        if not len(rows):
            return None

        doc = dict(zip(self.fields, rows[0]))
        logger.debug("GOT QUESTION:: {}".format(doc))
        return doc


class TriviaQuestionsMongo:
    con=None

    def __init__(self, host=MONGO_HOST, port=MONGO_PORT, db=MONGO_DB, coll_data=COLL_DATA):
        self.host = host
        self.port = port
        self.db = db
        self.coll_data = coll_data

    def connect(self):
        if self.con is None:
            self.con = MongoClient(self.host, self.port)

    def get_question(self):
        self.connect()
        col = self.con[self.db][self.coll_data]
        doc = None
        c = 0
        while doc is None and c < 10:
            c += 1
            index = random.randint(0, MAX_QUESTIONS)
            doc = col.find_one({'index': index})
        logger.debug("GOT QUESTION:: {}".format(doc))
        return doc


class TriviaQuestions:
    con = None

    def __init__(self, backend='sqlite', **kwargs):
        if backend=='sqlite':
            self.con = TriviaQuestionsSQLite(**kwargs)
        elif backend=='mongo':
            self.con = TriviaQuestionsMongo(**kwargs)

    def connect(self):
        pass

    def get_question(self):
        return self.con.get_question()
