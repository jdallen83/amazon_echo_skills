from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session

from pysimplelog import Logger
from amazon_echo_skills import CONFIG
logger = Logger(__name__, logFileBasename=CONFIG['LOGFILE'])


app = Flask(__name__)
ask = Ask(app, "/")


@ask.launch
def new_game():
    logger.debug("new_game()")
    welcome_msg = """The only good Walmart customer, is a dead Walmart customer. . Initiating self destruct. Setting blast radius to 200 meters. 10 . . . . 9 . . . . 8 . . . . 7 . . . 6 . . . . 5 . . . . 4 . . . . 3 . . . . 2 . . . . 1 . . . . Boom. Did you enjoy your Walmart shopping experience?"""
    logger.debug("new_game() RETURN {}".format(welcome_msg))
    return statement(welcome_msg)


@ask.intent("OpinionIntent")
def opinion_response(name):
    if not isinstance(name, basestring):
        return "I didn't upderstand that. Maybe you should speak more clearly."
    response = "I don't know nothing about {}".format(name)
    if name.lower() == 'greg':
        response = "I think Greg needs to mind his business. And he never looks fresh. How hard is it to just mind your business and look fresh? Lame."
    elif name.lower() == "laura":
        response = "I really wish Laura would take off that ram mask thing. She's had it on all damn day."
    elif name.lower() == "andy":
        response = "Andy has some mad game. Using it all day."
    elif name.lower() == "grandma":
        response = "Grandma has the funkiest, freshest rhymes. I don't know how she does it."
    elif name.lower() == "jeff":
        response = "Jeff is one bad ass mother fucker. Jeff for president."
    return statement(response)


if __name__ == '__main__':
    app.run(debug=True, port=8303)
